terminusmodx
============

terminusmodx font is the terminus 12pt pcf font with a bold version and status
icons patched for the [Powerline](https://github.com/Lokaltog/vim-powerline/)
vim plugin.

Credits
-------

Terminusmod is originally from [stlarch](http://sourceforge.net/users/stlarch)
and licensed under the [GPLv2](http://www.gnu.org/licenses/gpl-2.0.html)
license.

This repository is a fork of
[terminusmodx-powerline](https://github.com/ngquerol/terminusmodx-powerline)
from Nicolas G. Querol. It adds a working `fonts.dir` and more explanations.

How to use it?
--------------

### Installing terminusmodx

Run:

    :::bash
    mkdir -p $HOME/.fonts
    git clone git@bitbucket.org:halfr/terminusmodx.git $HOME/.fonts/terminusmodx
    # you might want to put the following lines in your .xinitrc or similar
    xset +fp $HOME/.fonts/terminusmodx
    xset fp rehash

### Using it in xterm

Add to your .Xdefaults/.Xressources:

    xterm*font:     -misc-terminusmodx-medium-r-normal--12-120-72-72-c-60-iso10646-1
    xterm*boldFont: -misc-terminusmodx-bold-r-normal--12-120-72-72-c-60-iso10646-1
